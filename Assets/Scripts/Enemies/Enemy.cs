﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Enemy : NetworkBehaviour {
	public Weapon myWeapon;
	public float attackMoveSpeed;
	public float walkMoveSpeed;
	public bool staticIdle;
	public bool randomIdle;
	public bool patrolIdle;

	private enum States{Idle, Alerted, Attacking, KnockedDown, LookingForWeapon, Dead};
	private float FOV;
	[SerializeField]
	private float lookAheadDistance = 5f;
	private float knockDownTimer;
	private float knockDownTimeMax = 5f;
	private float knockDownTimeMin = 3.5f;
	[SerializeField]
	private bool patrolLeft;
	[SyncVar]
	private Vector3 syncPos;
	[SyncVar]
	private Vector3 syncRot;
	private Point preSearchPoint;
	private Point searchPoint;
	private NetworkAnimator anim;
	private States state;
	private Transform myTransform;
	private MovementController moveCont;
	private Vector3 velocity;



	// Use this for initialization
	void Start () {
		knockDownTimer = Random.Range (knockDownTimeMin, knockDownTimeMax);
		myTransform = transform;
		state = States.Idle;
		moveCont = GetComponent<MovementController> ();
		moveCont.globalWaypoints [1] = transform.position;
		anim = GetComponent<NetworkAnimator> ();
		if (NetworkServer.active) {
			NetworkServer.Spawn (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (isServer) {
			if (state == States.Dead) {

			}else if (state == States.Attacking) {
				if (myWeapon != null) {

				} else {	//The Enemy has a melee weapon

				}
				if (transform.position != moveCont.globalWaypoints [1]) {
					if (state == States.Alerted) {
						velocity = moveCont.CalculateMovement (attackMoveSpeed);
					} else {
						velocity = moveCont.CalculateMovement (walkMoveSpeed);
					}
					transform.Translate (velocity);
				}
			} else if (state == States.Idle) {
				if (patrolIdle) {
					//UPDATE LAYER MASKS SO WALLS AND DOORS ARE THEIR OWN LAYER
					RaycastHit2D hit = Physics2D.Raycast (transform.position, transform.rotation, lookAheadDistance);
					if (hit) {
						if (hit.collider.tag == "TallObstacle" || hit.collider.tag == "ShortObstacle") {
							if (patrolLeft) {
								transform.Rotate (0, 0, 90);
							} else {
								transform.Rotate (0, 0, -90);
							}
						}
					}
				} else if (randomIdle) {

				} else if (staticIdle) {

				}
			} else if (state == States.Alerted) {
				//bc = PathFinder.FindPath (Grid.current, searchPoint, preSearchPoint);
			} else if (state == States.LookingForWeapon) {
				
			} else if (state == States.KnockedDown) {
				if (knockDownTimer > 0) {
					knockDownTimer -= Time.deltaTime;
				} else {
					knockDownTimer = Random.Range (knockDownTimeMin, knockDownTimeMax);
					state == States.LookingForWeapon;
					//SET ANIM TO UNARMED
				}

			}

		}
	}

	public void Investigate(Vector3 endPos){
		if (state == States.Idle) {
			preSearchPoint = new Point ((int)transform.position.x, (int)transform.position.y);
		}
		searchPoint = new Point((int)endPos.x, (int)endPos.y);
		state = States.Alerted;
		RaycastHit2D[] hits = Physics2D.LinecastAll (transform.position, endPos, 1 << 8);
		foreach (RaycastHit2D hit in hits){
			if (hit.collider.tag == "TallObstacle") {

			}
		}
		if (bc == null) {
			moveCont.globalWaypoints [0] = transform.position;
			moveCont.globalWaypoints [1] = endPos;
		}
	}

	/// <summary>
	/// Lerps the enemy position and rotation on every client
	/// </summary>
	void LerpEnemyPositions(){
		if (!isServer) {
			myTransform.position = Vector3.Lerp (myTransform.position, syncPos, Time.deltaTime * 15);
			myTransform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle (myTransform.localEulerAngles.z, syncRot.z, Time.deltaTime * 15));
		}
	}

	public void Die(){
		anim.SetTrigger ("dead");
		if (myWeapon != null) {
			myWeapon.CmdThrow (transform.position);
		}
		CmdSendEnemyDeathToServer (netId);
		this.enabled = false;
	}


	//After update is ran we want to send out our new position/rotation and smooth any other players movements
	void FixedUpdate(){
		if (isServer) {
			CmdTransmitPosition ();
		}
		LerpEnemyPositions ();
	}

	[Command]
	void CmdTransmitPosition(){
		syncPos = transform.position;
		syncRot = transform.localEulerAngles;
	}

	[Command]
	void CmdSendEnemyDeathToServer(NetworkInstanceId netId){
		RpcSendEnemyDeathToClients (netId);
	}

	[ClientRpc]
	void RpcSendEnemyDeathToClients(NetworkInstanceId netId){
		GameObject obj = ClientScene.FindLocalObject (netId);
		Enemy client = obj.GetComponent<Enemy> ();
		obj.GetComponent<BoxCollider2D> ().enabled = false;
		obj.GetComponent<SpriteRenderer> ().sortingOrder = -2;
		client.state = States.Dead;
		client.enabled = false;
	}
}
