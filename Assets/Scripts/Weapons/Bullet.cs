﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Bullet : NetworkBehaviour {
	public float moveSpeed;

	private float moveDist;
	private float bulletTime = 2f;
	private float bulletTimer;
	private Vector3 nextFramePos;
	private Vector3 lastFramePos;


	void Start(){
		bulletTimer = bulletTime;
	}

	public void BulletInit(Quaternion rot, float angle){
		transform.rotation = rot;
		transform.Rotate (0, 0, angle);
	}
	
	// Update is called once per frame
	void Update () {
		if (bulletTimer > 0) {
			bulletTimer -= Time.deltaTime;
			moveDist = moveSpeed * Time.deltaTime;
			nextFramePos = Vector3.up * moveDist;
			lastFramePos = transform.position;
			transform.Translate (nextFramePos);
			if (isServer) {
				RaycastHit2D hit = Physics2D.Linecast (lastFramePos, transform.position, 1 << 8);
				//Debug.DrawLine(lastFramePos, transform.position, Random.ColorHSV (), 5);
				if (hit) {
					if (hit.collider.tag == "Player") {
						Player player = hit.collider.GetComponent<Player> ();
						player.Die ();
					} else if (hit.collider.tag == "Enemy") {
						Enemy enemy = hit.collider.GetComponent<Enemy> ();
						enemy.Die ();
					} else if (hit.collider.tag == "tallObstacle") {
						//Hit a wall or door
					}
					bulletTimer = -1;
					transform.position = new Vector3 (hit.point.x, hit.point.y, 0);
				} 
			}
		} else {
			bulletTimer = bulletTime;
			ObjectPooler.current.UnspawnObject (gameObject);
			NetworkServer.UnSpawn (gameObject);
		}
	}
}