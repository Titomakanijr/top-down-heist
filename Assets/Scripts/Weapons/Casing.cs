﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Casing : MonoBehaviour {
	public float moveSpeedMax = 7f;
	public float moveSpeedMin = 5f;

	private float moveSpeed;
	private float moveDamping = .9f;
	private float rotationSpeedMax = 3f;
	private float rotation;
	private float bounceTime = .1f;
	private float bounceTimer;
	private bool groundBounce;

	void Start(){
		moveSpeed = Random.Range (moveSpeedMin, moveSpeedMax);
		rotation = Random.Range (-rotationSpeedMax, rotationSpeedMax);
		groundBounce = true;
	}

	// Update is called once per frame
	void Update () {
		if (moveSpeed >= 1f) {
			transform.Translate (Vector3.left * moveSpeed * Time.deltaTime);
			moveSpeed *= moveDamping;
			transform.Rotate (Vector3.forward * rotation);
		} else if(groundBounce){
			bounceTimer = bounceTime;
			groundBounce = false;
		}
		if (bounceTimer > 0) {
			bounceTimer -= Time.deltaTime;
			transform.Rotate (Vector3.forward * rotation * 1.5f);
		}
	}
}
