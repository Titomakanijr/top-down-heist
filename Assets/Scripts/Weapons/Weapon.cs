﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Weapon : NetworkBehaviour {
	public int maxAmmo;
	public int minAmmo;
	public int ammoCount;
	public int maxPellets;
	public int minPellets;
	public float spread;
	public float fireRate;
	public float fireTimer;
	public float shotSoundRadius;
	public bool isShotgun;
	public bool isMelee;
	public string weaponName;
	public GameObject casing;
	public GameObject bullet;
	public BoxCollider2D box;
	public SpriteRenderer spriteRend;
	public Rigidbody2D rig;


	private bool playerWeapon;
	private AudioSync audioSync;
	private Transform casingTrans;
	private NetworkHash128 bulletAssId;

	// Use this for initialization
	void Start () {
		box = GetComponent<BoxCollider2D> ();
		spriteRend = GetComponent<SpriteRenderer> ();
		rig = GetComponent<Rigidbody2D> ();
		ammoCount = Random.Range (minAmmo, maxAmmo);
		bulletAssId = bullet.GetComponent<NetworkIdentity> ().assetId;
		casingTrans = GameObject.Find ("Casings").transform;
		if (isServer) {
			NetworkServer.Spawn (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (audioSync == null) {
			audioSync = FindObjectOfType<AudioSync> ();
		}
		if (fireTimer > 0) {
			fireTimer -= Time.deltaTime;
		}
	}
		
	[Command]
	public void CmdSendShootToServer(Vector3 pos, Quaternion rot){
		ammoCount--;
		if (isShotgun) {
			int pellets = Random.Range (minPellets, maxPellets);
			for (int i = 0; i < pellets; i++) {
				GameObject newBullet = ObjectPooler.current.getPooledObject (pos, bulletAssId);
				NetworkServer.Spawn (newBullet, bulletAssId);
				RpcSendBulletInfoToClients(newBullet, rot, Random.Range (-spread, spread));
			}	
		} else {
			GameObject newBullet = ObjectPooler.current.getPooledObject (pos, bulletAssId);
			NetworkServer.Spawn (newBullet, bulletAssId);
			RpcSendBulletInfoToClients(newBullet, rot, Random.Range (-spread, spread));
		}
		GameObject newCasing = Instantiate (casing, pos, rot);
		newCasing.transform.parent = casingTrans;
		NetworkServer.Spawn (newCasing);
	}

	[ClientRpc]
	void RpcSendBulletInfoToClients(GameObject bul, Quaternion rot, float angle){
		Bullet obj = ClientScene.FindLocalObject(bul.GetComponent<NetworkIdentity>().netId).GetComponent<Bullet>();
		obj.BulletInit(rot, angle);
		if (playerWeapon && isServer) {
			Collider2D[] hits = Physics2D.OverlapCircleAll (obj.transform.position, shotSoundRadius);
			Enemy en;
			foreach (Collider2D hit in hits) {
				if (hit.tag == "Enemy") {
					en = hit.GetComponent<Enemy> ();
					en.Investigate (obj.transform.position);
				}
			}
		}
	}

	[Command]
	void CmdSendWeaponInfoToServer(int ammo, bool boxVal, bool spriteVal){
		RpcSendWeaponInfoToClients (ammo, boxVal, spriteVal);
	}

	[ClientRpc]
	void RpcSendWeaponInfoToClients(int ammo, bool boxVal, bool spriteVal){
		ammoCount = ammo;
		box.enabled = boxVal;
		spriteRend.enabled = spriteVal;
	}

	[Command]
	public void CmdPickedUp(bool isPlayer){
		RpcWeaponPickedUp (netId, isPlayer);
	}

	[ClientRpc]
	void RpcWeaponPickedUp(NetworkInstanceId id, bool isPlayer){
		Weapon weapon = ClientScene.FindLocalObject (id).GetComponent<Weapon> ();
		weapon.box.enabled = false;
		weapon.playerWeapon = isPlayer;
		weapon.spriteRend.enabled = false;
	}

	[Command]
	public void CmdThrow(Vector3 pos){
		RpcWeaponthrown (netId, pos, ammoCount);
	}

	[ClientRpc]
	void RpcWeaponthrown(NetworkInstanceId id, Vector3 pos, int ammo){
		Weapon weapon = ClientScene.FindLocalObject (id).GetComponent<Weapon> ();
		weapon.transform.position = pos;
		weapon.rig.AddForce (Vector2.one * 5);
		weapon.box.enabled = true;
		weapon.spriteRend.enabled = true;
		weapon.ammoCount = ammo;
	}

	void OnDrawGizmos(){
		Gizmos.DrawWireSphere (transform.position, shotSoundRadius);
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(Weapon))]
public class Weapon_Editor : Editor{
	public override void OnInspectorGUI(){
		Weapon script = (Weapon)target;
		script.weaponName = EditorGUILayout.TextField ("Weapon Name", script.weaponName);

		//This is a gun
		if (!script.isMelee) {
			script.isShotgun = EditorGUILayout.Toggle ("Is Shotgun", script.isShotgun);
			script.spread = EditorGUILayout.FloatField ("Spread", script.spread);
			script.maxAmmo = EditorGUILayout.IntField ("Max Ammo Count", script.maxAmmo);
			script.minAmmo = EditorGUILayout.IntField ("Min Ammo Count", script.minAmmo);
			script.fireRate = EditorGUILayout.FloatField ("Fire Rate", script.fireRate);
			script.shotSoundRadius = EditorGUILayout.FloatField ("Shot Sound Radius", script.shotSoundRadius);
			script.bullet = EditorGUILayout.ObjectField ("Bullet Prefab", script.bullet, typeof(GameObject)) as GameObject;
			script.casing = EditorGUILayout.ObjectField ("Casing Prefab", script.casing, typeof(GameObject)) as GameObject;
		} 

		if (!script.isShotgun) {
			script.isMelee = EditorGUILayout.Toggle ("Is Melee", script.isMelee);
		}

		if (script.isShotgun) {
			script.maxPellets = EditorGUILayout.IntField ("Max Pellets", script.maxPellets);
			script.minPellets = EditorGUILayout.IntField ("Min Pellets", script.minPellets);
		}
	}
}
#endif