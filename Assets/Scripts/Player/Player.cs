﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player : NetworkBehaviour {
	public float moveSpeed = 5f;
	public float maxCamLookAhead = 6f;
	[HideInInspector]
	public bool dead;
	public Transform xHairTrans;

	private int clientAmmo;
	[SyncVar]
	private Vector3 syncPos;
	[SyncVar]
	private Vector3 syncRot;
	private Transform myTransform;
	private Transform gunBarrel;
	private SpriteRenderer spriteRend;
	private Camera mainCam;
	private Weapon myWeapon;
	private NetworkAnimator anim;
	private AudioSync netAudio;
	private Text ammoText;

	// Use this for initialization
	void Start() {
		dead = false;
		mainCam = FindObjectOfType<Camera> ();
		myTransform = GetComponent<Transform> ();
		anim = GetComponent<NetworkAnimator> ();
		netAudio = GetComponent<AudioSync> ();
		ammoText = GameObject.Find ("Ammo Text").GetComponent<Text> ();
		gunBarrel = transform.GetChild (1);
	}
	
	// Update is called once per frame
	void Update () {

		//We don't want to run any update code for non-local player
		if (!isLocalPlayer) {
			return;
		}

		if (dead) {
			return;
		}

		anim.animator.ResetTrigger ("attack");

		if (myWeapon != null){
			if (Input.GetMouseButton (0)) {
				if (myWeapon.fireTimer <= 0) {
					if (clientAmmo <= 0) {
						netAudio.CmdSendAudioToServer (transform.position, "gunClick");
					} else {
						anim.SetTrigger ("attack");
						anim.animator.SetTrigger ("attack");
						netAudio.CmdSendAudioToServer (transform.position, myWeapon.weaponName);
						//POSSIBLY CREATE ANOTHER CASING OBJECT AT DIFFERENT POS
						myWeapon.CmdSendShootToServer(gunBarrel.position, myTransform.rotation);
						myWeapon.fireTimer = myWeapon.fireRate;
						clientAmmo--;
						ammoText.text = clientAmmo + " Rounds";
					}
				}
			} else if(Input.GetMouseButtonDown(1)) {
				CmdThrowWeapon (myWeapon.gameObject);
				myWeapon = null;
				anim.SetTrigger ("unarmed");
				ammoText.text = "";
				//UPDATE ANIMATIONS HERE!
			}
		}else if(Input.GetMouseButtonDown(1)){
			Collider2D[] cols = Physics2D.OverlapBoxAll (new Vector2(myTransform.position.x, myTransform.position.y), new Vector2(.5f, .5f), 0);
			foreach (Collider2D col in cols) {
				if (col.tag == "Weapon") {
					myWeapon = col.GetComponent<Weapon> ();
					clientAmmo = myWeapon.ammoCount;
					ammoText.text = clientAmmo + " Rounds";
					anim.SetTrigger (myWeapon.weaponName);
					CmdPickUpWeapon (myWeapon.gameObject);
					break;
				}
			}

		}

		//Get player movement input
		Vector2 input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));

		//update crosshair position to current mouse position
		Vector3 mousePos = Input.mousePosition;
		xHairTrans.position = mainCam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10));

		//Update player rotation to face crosshair
		myTransform.localEulerAngles = new Vector3(0, 0, Mathf.Atan2((xHairTrans.position.y - myTransform.position.y), (xHairTrans.position.x - myTransform.position.x)) * Mathf.Rad2Deg - 90);

		//Calculate players velocity for this frame based on input
		Vector3 velocity = new Vector3 (input.x * moveSpeed * Time.deltaTime, input.y * moveSpeed * Time.deltaTime, 0);
		transform.Translate (velocity, Space.World);

		//Camera look around
		if (Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift)) {
			Vector3 camLook = xHairTrans.position;
			camLook.z = -10f;
			//camera can move as long as it is not beyond the max look ahead distance
			if ((Vector2.Distance(myTransform.position, mainCam.transform.position) < maxCamLookAhead) || 
						(Vector2.Distance(myTransform.position, xHairTrans.position) < maxCamLookAhead)) {
				mainCam.transform.Translate ((camLook - mainCam.transform.position) * 2 * Time.deltaTime);
			} 
		//If look around has ended we want the camera to reset to the player
		} else if(mainCam.transform.position.x != myTransform.position.x || mainCam.transform.position.y != myTransform.position.y) {
			mainCam.transform.position = Vector3.Lerp(new Vector3(myTransform.position.x, myTransform.position.y, -10f), mainCam.transform.position, Time.deltaTime * 15);
		}
		//Move camera along with the player's velocity
		mainCam.transform.Translate (velocity, Space.World);

	}

	public void Die(){
		CmdSendPlayerDeathToServer (netId);
		if (myWeapon != null) {
			CmdThrowWeapon (myWeapon.gameObject);
		}
		anim.SetTrigger ("dead");
	}

	public override void OnStartLocalPlayer ()
	{
		spriteRend = GetComponent<SpriteRenderer> ();
		spriteRend.color = Color.green;
		xHairTrans.gameObject.SetActive (true);
	}

	void LerpPlayerPositions(){
		if (!isLocalPlayer) {
			myTransform.position = Vector3.Lerp (myTransform.position, syncPos, Time.deltaTime * 15);
			myTransform.localEulerAngles = new Vector3(0, 0, Mathf.LerpAngle (myTransform.localEulerAngles.z, syncRot.z, Time.deltaTime * 15));
		}
	}

	//After update is ran we want to send out our new position/rotation and smooth any other players movements
	void FixedUpdate(){
		TransmitPosition();
		LerpPlayerPositions ();
	}

	[Command]
	void CmdSendPlayerDeathToServer(NetworkInstanceId netId){
		RpcSendPlayerDeathToClients (netId);
	}

	[ClientRpc]
	void RpcSendPlayerDeathToClients(NetworkInstanceId netId){
		GameObject obj = ClientScene.FindLocalObject (netId);
		Player client = obj.GetComponent<Player> ();
		client.GetComponent<BoxCollider2D> ().enabled = false;
		client.GetComponent<SpriteRenderer> ().sortingOrder = -2;
		client.dead = true;
	}

	//Send position and rotation to the server
	[Command]
	void CmdTransformToServer(Vector3 pos, Vector3 rot){
		syncPos = pos;
		syncRot = rot;
	}

	//Called by each client to send transform information to server
	[ClientCallback]
	void TransmitPosition(){
		if (isLocalPlayer) {
			CmdTransformToServer (myTransform.position, myTransform.localEulerAngles);
		}
	}

	[Command]
	void CmdPickUpWeapon(GameObject obj){
		NetworkInstanceId netId = obj.GetComponent<NetworkIdentity> ().netId;
		GameObject client = NetworkServer.FindLocalObject (netId);
		NetworkIdentity ni = client.GetComponent<NetworkIdentity> ();
		ni.AssignClientAuthority (connectionToClient);
		Weapon tempWep = ClientScene.FindLocalObject (netId).GetComponent<Weapon> ();
		tempWep.CmdPickedUp(true);
	}

	[Command]
	void CmdThrowWeapon(GameObject obj){
		NetworkInstanceId netId = obj.GetComponent<NetworkIdentity> ().netId;
		GameObject client = NetworkServer.FindLocalObject (netId);
		NetworkIdentity ni = client.GetComponent<NetworkIdentity> ();
		ni.RemoveClientAuthority (ni.clientAuthorityOwner);
		Weapon tempWep = ClientScene.FindLocalObject (netId).GetComponent<Weapon> ();
		tempWep.CmdThrow (myTransform.position);
	}
}
