﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ObjectPooler : MonoBehaviour {

	public static ObjectPooler current;
	public GameObject[] prefabs;
	public int pooledAmount;

	private List<List<GameObject>> pools;
	private Dictionary<NetworkHash128, int> objectDict;

	void Awake(){
		current = this;
	}

	// Use this for initialization
	void Start () {
		NetworkHash128 assId;
		int j = 0;
		pools = new List<List<GameObject>>();
		objectDict = new Dictionary<NetworkHash128, int> ();
		foreach (GameObject prefab in prefabs) {
			List<GameObject> newList = new List<GameObject> ();
			for (int i = 0; i < pooledAmount; i++) {
				GameObject obj = (GameObject)Instantiate (prefab);
				obj.transform.parent = transform;
				obj.SetActive (false);
				newList.Add (obj);
			}
			pools.Add (newList);
			assId = prefab.GetComponent<NetworkIdentity> ().assetId;
			objectDict.Add (assId, j);
			ClientScene.RegisterSpawnHandler (assId, SpawnObject, UnspawnObject);
			j++;
		}
	}

	public GameObject getPooledObject(Vector3 pos, NetworkHash128 assId){
		int index;
		GameObject obj;
		objectDict.TryGetValue (assId, out index);
		for (int i = 0; i < pooledAmount; i++) {
			if (!pools [index] [i].activeInHierarchy) {
				obj = pools [index] [i];
				obj.transform.position = pos;
				obj.SetActive (true);
				return obj;
			}
		}

		//If there was not a pooled object we are going to want to create a new one
		obj = (GameObject)Instantiate (prefabs [index]);
		obj.transform.parent = transform;
		pools [index].Add (obj);
		obj.transform.position = pos;
		obj.SetActive (true);
		return obj;
	}

	public GameObject SpawnObject(Vector3 pos, NetworkHash128 assId){
		return getPooledObject (pos, assId);
	}

	public void UnspawnObject(GameObject spawned){
		spawned.SetActive (false);
	}
}
