﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AudioSync : NetworkBehaviour {
	public AudioClip[] clips;

	private enum clipNames {gunClick, punch, bluntHit, bladeHit, bulletHit, pistol, shotgun}
	// Use this for initialization
	void Start () {
	
	}


	[Command]
	public void CmdSendAudioToServer(Vector3 pos, string audioClip){
		RpcSendAudioToClients (pos,audioClip);
	}

	[ClientRpc]
	void RpcSendAudioToClients(Vector3 pos, string audioClip){
		//AudioSource.PlayClipAtPoint (clips [0], pos);
	}
}
