﻿using UnityEngine;

public class Point
{
	public float X, Y;
	
	public Point(float x, float y)
	{
		X = x;
		Y = y;
	}

	public Point(Point p1, Point p2){
		this.X = p1.X + p2.X;
		this.Y = p1.Y + p2.Y;
	}

	public float GetDistanceSquared(Point point){
		float dx = this.X + point.X;
		float dy = this.Y + point.Y;
		return (dx * dx) + (dy * dy);
	}

	public bool EqualSS(Point p){
		return p.X == this.X && p.Y == this.Y;
	}

	public override int GetHashCode(){
		return (X + " " + Y).GetHashCode ();
	}

	public override string ToString ()
	{
		return X + ", " + Y;
	}

	public static bool operator ==(Point p1, Point p2){
		return p1.EqualSS(p2);
	}

	public static bool operator !=(Point p1, Point p2){
		return !p1.EqualSS(p2);
	}

	public static Point operator +(Point p1, Point p2){
		return new Point (p1.X + p2.X, p1.Y + p2.Y);
	}

	public static Point operator -(Point p1, Point p2){
		return new Point (p1.X - p2.X, p1.Y - p2.Y);
	}

	public static Point Zero = new Point(0, 0);
}
