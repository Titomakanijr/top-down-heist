﻿using UnityEngine;

public static class PathFinder
{     
	public static SearchNode FindPath(World world, Point start, Point end)
	{        
	   	return FindPathReversed(world, end, start);
	}
	
	private static SearchNode FindPathReversed(Grid world, Point start, Point end)
	{

		SearchNode startNode = new SearchNode (start, 0, 0, null);

		MinHeap openList = new MinHeap();
		openList.Add(startNode);

		int sx = world.Right;
		int sy = world.Top;
		bool[] brWorld = new bool[world.Right, world.Top];
		brWorld [start.X + (start.Y * sx * sy)] = true;
	
		while (openList.HasNext())
	    {
	        //Find best item and switch it to the 'closedList'
	        SearchNode current = openList.ExtractFirst();

			if (current.position.GetDistanceSquared (end) <= 3) {
				return new SearchNode (end, current.pathCost + 1, current.cost, current);
			}
	
	        //Find neighbours
	        for (int i = 0; i < surrounding.Length; i++)
	        {
                tmp = new Point(current.position.X + surrounding[i].X, current.position.Y + surrounding[i].Y);

                if (!world.ConnectionIsValid(current.position, tmp)) 
                    continue;                				                  

                //Check if we've already examined a neighbour, if not create a new node for it.
                if (brWorld[tmp.X, tmp.Y] == null)
                {
                    node = new BreadCrumb(tmp);
                    brWorld[tmp.X, tmp.Y] = node;
                }
                else
                {
                    node = brWorld[tmp.X, tmp.Y];
                }

                //If the node is not on the 'closedList' check it's new score, keep the best
                if (!node.onClosedList)
                {
                    diff = 0;
                    if (current.position.X != node.position.X)
                    {
                        diff += 1;
                    }
                    if (current.position.Y != node.position.Y)
                    {
                        diff += 1;
                    }

					int distance = (int)Mathf.Pow(Mathf.Max(Mathf.Abs (end.X - node.position.X), Mathf.Abs(end.Y - node.position.Y)), 2);
                    cost = current.cost + diff + distance;

                    if (cost < node.cost)
                    {
                        node.cost = cost;
                        node.next = current;
                    }

                    //If the node wasn't on the openList yet, add it 
                    if (!node.onOpenList)
                    {
                        //Check to see if we're done
                        if (node.Equals(finish))
                        {
                            node.next = current;
                            return node;
                        }
                        node.onOpenList = true;
                        openList.Add(node);
                    }
	            }
	        }
	    }
	    return null; //no path found
	}
	
	//Neighbour options
	//Our diamond pattern offsets top/bottom/left/right by 2 instead of 1
	private static Point[] surrounding = new Point[]{                         
		new Point(0, 2), new Point(-2, 0), new Point(2, 0), new Point(0,-2),	
        new Point(-1, 1), new Point(-1, -1), new Point(1, 1), new Point(1, -1)
	};
}

