﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SearchNode
{
    public Vector2 position;
    public float cost;
    public float pathCost;
    public SearchNode next;
    public SearchNode nextListElem;

    public SearchNode(Vector2 position, float cost, float pathCost, SearchNode next)
    {
        this.position = position;
        this.cost = cost;
        this.pathCost = pathCost;
        this.next = next;
    }
}
